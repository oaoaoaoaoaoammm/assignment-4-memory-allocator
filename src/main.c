
#include "test.h"

int main() {

    if (!first_test()) {
        printf("Test 1 failed\n");
        return 1;
    }
    if (!second_test()) {
        printf("Test 2 failed\n");
        return 1;
    }

    return 0;
}
