#include "test.h"

#define HEAP 800
#define FIRST_ALLOC 200
#define SECOND_ALLOC 400

int first_test() {
    printf("Test 1 start\n");
    struct block_header* heap = (struct block_header*) heap_init(HEAP);
    if (!heap){
        return 1;
    }
    void* mem_alloc = _malloc(FIRST_ALLOC);
    debug_heap(stdout, heap);
    _free(mem_alloc);
    printf("Test 1 finish\n");
    return 0;
}

int second_test() {
    printf("Test 2 start\n");
    struct block_header* heap = (struct block_header*) heap_init(HEAP);
    if (!heap){
        return 1;
    }
    void* mem_alloc_first = _malloc(FIRST_ALLOC);
    void* mem_alloc_second = _malloc(SECOND_ALLOC);

    debug_heap(stdout, heap);
    _free(mem_alloc_first);
    debug_heap(stdout, heap);
    _free(mem_alloc_second);
    printf("Test 2 finish\n");
    return 0;
}
